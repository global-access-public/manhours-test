{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Server where

import Api (API, Allocation (Allocation), Change (Change), Hour)
import Control.Lens (Ixed (ix), (^..))
import qualified Data.Map.Strict as M
import Data.Time (Day, getCurrentTime)
import Data.Time.Lens (day, modL)
import Database (Database, mkState, run)
import Docs (docsBS)
import Network.HTTP.Types (ok200)
import Network.Wai (responseLBS)
import qualified Network.Wai.Handler.Warp as W
import Protolude hiding (Handler) 
import Servant
  ( Handler
  , Raw
  , Server
  , Tagged (Tagged)
  , serve
  , (:<|>) ((:<|>))
  , type (:>)
  )
import Turtle.Options (Parser, optInt, options)

server :: Database -> Server API
server (ps, as) = changes :<|> allocations
  where
    changes :: Day -> Handler [Change]
    changes d =
      return do
        pes <- ps ^.. ix d
        (c, p, e) <- pes
        pure $ Change p c e

    allocations :: Hour -> Handler [Allocation]
    allocations hour =
      return do
        wps <- as ^.. ix hour
        (w, (c, p)) <- M.assocs wps
        pure $ Allocation p c w

serverWithDocs :: Database -> Server WithDocs
serverWithDocs db = server db :<|> Tagged serveDocs
  where
    serveDocs _ respond =
      respond $ responseLBS ok200 [plain] docsBS
    plain = ("Content-Type", "text/html")

application :: Database -> IO ()
application = W.run 8081 . serve (Proxy @WithDocs) . serverWithDocs

type WithDocs = API :<|> "docs" :> Raw

parser :: Parser (Int, Int, Int, Int, Int, Int)
parser =
  (,,,,,)
    <$> optInt "seed" 's' "random seed"
    <*> optInt "workers" 'w' "number of workers"
    <*> optInt "customers" 'c' "number of customers"
    <*> optInt "projects" 'p' "number of projects"
    <*> optInt "allocations" 'a' "number of worker to project allocations"
    <*> optInt "days" 'd' "number of days (back from today) to go"

main :: IO ()
main = do
  (s, w, c, p, a, d) <- options "Test data server" parser
  end <- getCurrentTime
  let start = modL day (subtract d) end
      database = run s $ mkState w c p a start end
  putText "buiding database.."
  seq (snd database) $ pure ()
  putText "ready"
  application database
