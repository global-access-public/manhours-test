{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wno-orphans #-}


module Docs where

import Api (API, Allocation (Allocation), Change (Change), Event (Start, Stop), Hour, Project (Project), Customer (Customer), Worker (Worker))
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.Text.Lazy as TL
import Data.Time (Day)
import Protolude 
import Servant ( Required, Strict, QueryParam' )
import qualified Servant.Docs as D
import qualified Text.Blaze.Html.Renderer.Pretty as BZ
import Text.Markdown (def)
import qualified Text.Markdown as MD

apiDocs :: D.API
apiDocs = D.docs do Proxy @API

instance D.ToParam (QueryParam' '[Required, Strict] "day" Day) where
  toParam _ =
    D.DocQueryParam
      "day" -- name
      ["2022-04-01"] -- example of values (not necessarily exhaustive)
      "The day you want data" -- description
      D.Normal -- Normal, List or Flag

instance D.ToParam (QueryParam' '[Required, Strict] "hour" Hour) where
  toParam _ =
    D.DocQueryParam
      "hour" -- name
      ["2015-10-03T01:00:00Z"] -- example of values (not necessarily exhaustive)
      "The hour you want data, any UTCTime inside an  hour will match the same hour" -- description
      D.Normal -- Normal, List or Flag

instance D.ToSample Change where
  toSamples _ =
    [ ("A start event", Change (Project 1) (Customer 1) Start)
    , ("A stop event", Change (Project 1) (Customer 1) Stop)
    ]

instance D.ToSample Allocation where
  toSamples _ =
    [ ("allocate woker 1 to project 1 for customer 1", Allocation (Project 1) (Customer 1) (Worker 1))
    , ("allocate woker 1 to project 1 for customer 2", Allocation (Project 1) (Customer 2) (Worker 1))
    ]

docsBS :: BL.ByteString
docsBS = BL.pack . BZ.renderHtml . MD.markdown def
  . TL.pack
  . D.markdown
  $ D.docsWithIntros [intro] do Proxy @API
  where
    intro = D.DocIntro "Welcome" ["This is a data provider for a test.", "Enjoy!"]
