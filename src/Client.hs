{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Client where

import Api (API, Allocation (Allocation), Change (Change), Customer (Customer), Event (Start, Stop), Hour (Hour), Project, Worker (Worker))
import Control.Arrow ((&&&))
import Control.Lens (over, view, _Wrapped)
import Data.Map.Monoidal.Strict (MonoidalMap)
import qualified Data.Map.Monoidal.Strict as Mm
import Data.Semigroup (Arg (Arg), Max (Max))
import qualified Data.Set as Set
import Data.Time (Day, UTCTime (UTCTime, utctDay), getCurrentTime)
import Data.Time.Lens
  ( day
  , getL
  , hours
  , minutes
  , modL
  , month
  , seconds
  , setL
  , year
  )
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Protolude
import Servant ((:<|>) ((:<|>)))
import Servant.Client (ClientM, client, mkClientEnv, parseBaseUrl, runClientM)
import Streaming (MFunctor (hoist), Of, Stream, distribute)
import qualified Streaming.Prelude as S
import Turtle.Options (Parser, optInt, options, switch)
import Prelude hiding (sum, undefined)

getChanges :: Day -> ClientM [Change]
getAllocations :: Hour -> ClientM [Allocation]
getChanges :<|> getAllocations = client do Proxy @API

streamChanges :: Day -> Day -> Stream (Of (Day, [Change])) ClientM ()
streamChanges lastDay d
  | d <= lastDay = do
    lift (getChanges d) >>= S.yield . (d,)
    streamChanges lastDay $ succ d
  | otherwise = pure ()

streamAllocations :: Hour -> Hour -> Stream (Of (Hour, [Allocation])) ClientM ()
streamAllocations lastHour h
  | h <= lastHour = do
    lift (getAllocations h) >>= S.yield . (h,)
    streamAllocations lastHour $ over _Wrapped (modL hours succ) h
  | otherwise = pure ()

projectsSet
  :: Monad m
  => Stream (Of (Day, [Change])) m ()
  -> Stream (Of Hour) (StateT (Set (Customer, Project)) m) ()
projectsSet = S.map upsample . S.mapM change . hoist lift
  where
    change :: Monad m => (Day, [Change]) -> StateT (Set (Customer, Project)) m Day
    change (d, cs) = do
      forM_ cs \(Change p c e) -> do
        modify $ case e of
          Start -> Set.insert (c, p)
          Stop -> Set.delete (c, p)
      pure d
    upsample :: Day -> Hour
    upsample d = Hour $ UTCTime d 0

allocationsCount
  :: Monad m
  => Stream (Of (Hour, [Allocation])) m ()
  -> Stream (Of (Hour, MonoidalMap (Customer, Project) (Set Worker))) m ()
allocationsCount = S.map $
  second $ foldMap
    do \(Allocation p c w) -> Mm.singleton (c, p) $ Set.singleton w

validateAllocations
  :: forall m.
  Monad m
  => Stream (Of Hour) (StateT (Set (Customer, Project)) m) ()
  -> Stream (Of (Hour, MonoidalMap (Customer, Project) (Set Worker))) m ()
  -> Stream (Of (Hour, MonoidalMap (Customer, Project) (Set Worker))) (Stream (Of (Hour, Worker)) m) ()
validateAllocations projectsSet' allocationsCount' =
  void
    . flip evalStateT mempty
    . distribute
    . S.mapM keepValid
    . hoist (hoist lift)
    $ S.mergeOn
      do fst
      do S.map (,mempty) projectsSet'
      do hoist lift allocationsCount'
  where
    keepValid
      :: (Hour, MonoidalMap (Customer, Project) (Set Worker))
      -> StateT
           (Set (Customer, Project))
           (Stream (Of (Hour, Worker)) m)
           (Hour, MonoidalMap (Customer, Project) (Set Worker))
    keepValid (h, m) = do
      ks <- get
      lift $ S.map (h,) $ traverse_ S.each $ Mm.withoutKeys m ks
      pure (h, Mm.restrictKeys m ks)

collapseCustomer
  :: MonoidalMap (Customer, Project) (Set Worker)
  -> MonoidalMap Customer (MonoidalMap Project (Set Worker))
collapseCustomer m = fold do
  ((c, p), s) <- Mm.assocs m
  pure $ Mm.singleton c $ Mm.singleton p s

groupByMonth
  :: (Monad m, Monoid w)
  => ((Hour, b) -> w)
  -> Stream (Of (Hour, b)) m r
  -> Stream (Of w) m r
groupByMonth f =
  S.mapped do S.foldMap f
    . S.groupBy do (==) `on` (getL year &&& getL month) . view _Wrapped . fst

parser :: Parser (Int, Bool)
parser =
  (,)
    <$> optInt "days" 'd' "number of days (back from today) to go"
    <*> switch "bonus" 'b' "if to compute the bonus"

main :: IO ()
main = void $ do
  (days, bonus) <- options "Test data client" parser
  now <- getCurrentTime
  let today = utctDay now
      startDay = modL day (subtract days) today
      thisHour = Hour . setL minutes 0 . setL seconds 0 $ now
      startHour = over _Wrapped (modL day $ subtract days) thisHour
  manager <- newManager defaultManagerSettings
  url <- parseBaseUrl "http://localhost:8081"
  runClientM
    do
      (if bonus then S.print else S.effects)
        . S.map do \(Last (Just h), Just (Max (Arg (Sum v) k))) -> ("champion" :: Text, getL year h, getL month h, k, v)
        . S.map do second $ Mm.foldMapWithKey do { \k v -> Just $ Max (Arg v k) }
        . groupByMonth do \(Hour h, Worker w) -> (Last (Just h), Mm.singleton w (Sum @Int 1))
        . do
          if bonus
            then S.print . S.map do \(y, m, c, n) -> ("man-hours" :: Text, y, m, c, n)
            else S.print
        $ flip
          S.for
          do
            \(Last (Just (Hour h)), m) ->
              forM_ (Mm.assocs m) \(Customer c, Sum n) ->
                S.yield (getL year h, getL month h, c, n)
          do
            groupByMonth do first (Last . Just)
              . S.map do second $ fmap (sum . fmap (Sum . length)) . collapseCustomer
              $ validateAllocations
                do projectsSet $ streamChanges today startDay
                do allocationsCount $ streamAllocations thisHour startHour
    do mkClientEnv manager url
