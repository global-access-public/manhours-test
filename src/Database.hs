{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Database where

import Api (Customer (Customer), Event (..), Hour (Hour), Project (Project), Worker (Worker))
import Control.Lens (at, over, use, (%=), _Wrapped)
import Data.Map.Monoidal.Strict (MonoidalMap)
import qualified Data.Map.Monoidal.Strict as Mm
import qualified Data.Map.Strict as M
import Data.Time (Day, UTCTime (utctDay), addUTCTime)
import Protolude
import QuickCheck.GenT (GenT, MonadGen, choose, elements, runGenT)
import qualified Streaming.Prelude as S
import Test.QuickCheck.Gen (Gen (unGen))
import Test.QuickCheck.Random (mkQCGen)

type Projects = MonoidalMap Day [(Customer, Project, Event)]

projectsG :: [Customer] -> UTCTime -> UTCTime -> GenT (State (Map Customer Int)) Projects
projectsG cs (fromEnum . utctDay -> d0) (fromEnum . utctDay -> d1) = do
  customer <- elements cs
  project :: Int <- fmap (fromMaybe 1) . lift $ use $ at customer
  lift $ at customer %= Just . maybe 2 succ
  let interval d = do
        start <- lift $ choose (d, d1)
        span <- lift $ choose (0, fromEnum d1 - fromEnum d0)
        let end = start + span
        S.yield $
          Mm.fromList
            [ (toEnum start, [(customer, Project project, Start)])
            , (toEnum end, [(customer, Project project, Stop)])
            ]
        if end  + 1 <= d1 then interval (end + 1) else pure ()
  S.foldMap_ identity  $ interval d0

type Allocations = MonoidalMap Hour (Map Worker (Customer, Project))

allocationsG :: MonadGen m => [Worker] -> [(Customer, Project)] -> Hour -> Hour -> m Allocations
allocationsG ws ps h0 h1 = do
  w <- elements ws
  p <- elements ps
  h <- elements $ takeWhile (<= h1) $ iterate (over _Wrapped $ addUTCTime 3600) h0
  pure $ Mm.singleton h (M.singleton w p)

type Database = (Projects, Allocations)

mkState
  :: Int -- ^ number of workers
  -> Int -- ^ number of customers
  -> Int -- ^ number of projects
  -> Int -- ^ number of allocations
  -> UTCTime -- ^ start of time
  -> UTCTime -- ^ end of time
  -> GenT (State (Map Customer Int)) Database
mkState nw nc np na u0 u1 = do
  let customers = Customer <$> [1 .. nc]
      workers = Worker <$> [1 .. nw]
  projects <- fmap fold $ replicateM np $ projectsG customers u0 u1
  allocations <-
    fmap fold $
      replicateM na $
        allocationsG
          workers
          ((\(c, p, _) -> (c, p)) <$> fold projects)
          (Hour u0)
          (Hour u1)
  pure (projects, allocations)

run :: Int -> GenT (State (Map Customer Int)) a -> a
run v f = evalState (unGen (runGenT f) (mkQCGen v) v) mempty
