{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Api where

import Control.Lens (makeWrapped)
import Data.Aeson
  ( FromJSON (parseJSON)
  , Options (fieldLabelModifier)
  , ToJSON (toEncoding, toJSON)
  , defaultOptions
  , genericParseJSON
  , genericToEncoding
  , genericToJSON
  )
import Data.List (tail)
import Data.Time (UTCTime)
import Data.Time.Lens (Day, minutes, seconds, setL)
import Protolude
import Servant.API
  ( FromHttpApiData
  , Get
  , JSON
  , QueryParam'
  , Required
  , Strict
  , ToHttpApiData
  , type (:<|>)
  , type (:>)
  )

newtype Project = Project Int
  deriving (Generic, Show, Eq, Ord, ToJSON, FromJSON)

newtype Worker = Worker Int
  deriving (Generic, Show, Eq, Ord, ToJSON, FromJSON)

newtype Customer = Customer Int
  deriving (Generic, Show, Eq, Ord, ToJSON, FromJSON)

data Event = Start | Stop
  deriving (Generic, Show, Eq, Enum)

instance ToJSON Event

instance FromJSON Event

data Change = Change
  { change_project :: Project
  , change_customer :: Customer
  , change_event :: Event
  }
  deriving (Generic, Show, Eq)

customOptions :: Options
customOptions =
  defaultOptions
    { fieldLabelModifier = tail . dropWhile (/= '_')
    }

instance ToJSON Change where
  toJSON = genericToJSON customOptions
  toEncoding = genericToEncoding customOptions

instance FromJSON Change where
  parseJSON = genericParseJSON customOptions

newtype Hour = Hour {getHourTime :: UTCTime}
  deriving (Generic, Show, FromHttpApiData, ToHttpApiData)

makeWrapped ''Hour

instance Eq Hour where
  (==) = (==) `on` setL seconds 0 . setL minutes 0 . getHourTime

instance Ord Hour where
  compare = compare `on` setL seconds 0 . setL minutes 0 . getHourTime

instance ToJSON Hour

instance FromJSON Hour

data Allocation = Allocation
  { allocation_project :: Project
  , allocation_customer :: Customer
  , allocation_worker :: Worker
  }
  deriving (Generic, Show, Eq)

instance ToJSON Allocation where
  toJSON = genericToJSON customOptions
  toEncoding = genericToEncoding customOptions

instance FromJSON Allocation where
  parseJSON = genericParseJSON customOptions

type API =
  "projects" :> QueryParam' '[Required, Strict] "day" Day :> Get '[JSON] [Change]
    :<|> "allocations" :> QueryParam' '[Required, Strict] "hour" Hour :> Get '[JSON] [Allocation]
