# manhours-challenge



## build this code

* download [stack](https://docs.haskellstack.org/en/stable/README/)
*  ```bash
   stack install 
   ```

## local http server on port 8081


```shell 
manhours-server -s0 -w100 -c5 -p20 -a100000 -d90 
```

### server options

```bash
Test data server

Usage: manhours-test (-s|--seed SEED) (-w|--workers WORKERS)
                     (-c|--customers CUSTOMERS) (-p|--projects PROJECTS)
                     (-a|--allocations ALLOCATIONS) (-d|--days DAYS)

Available options:
  -h,--help                Show this help text
  -s,--seed SEED           random seed
  -w,--workers WORKERS     number of workers
  -c,--customers CUSTOMERS number of customers
  -p,--projects PROJECTS   number of projects
  -a,--allocations ALLOCATIONS
                           number of worker to project allocations
  -d,--days DAYS           number of days (back from today) to go
```

### service endpoints 

* see the API endpoints at http://localhost:8081/docs 
* get hourly allocations at http://localhost:8081/allocations?hour=2022-04-05T00:14:24Z 
* get daily project changes at http://localhost:8081/projects?day=2022-04-18

## the challange 

write an http client that query both endpoints and collect the monthly number of hours for each customer, by counting the valid worked hours (relatively to start and stop day of each project) each worker did for each customer 

* consider that project name is not unique among different customers
* consider that projects may by opened and closed more than once
* do not pre-load all the hourly allocation, compute in a streaming fashon
* do not pre-load all the project changes, interrogate days as your hour time counter ticks
* compute all the previous months values even for partial months, use at least a  `-d 90` in the server 
* produce a stream of tuples each with (year, month, customer, worked hours) where 
    > worked hours = count of all valid worked hours for the month for each active project for the customer 


### output for the client run at `lun 25 apr 2022, 16:16:48, CEST` 


```haskell
-- (year, month, customer, worked hours)

(2022,1,2,369)
(2022,2,1,2774)
(2022,2,2,1846)
(2022,2,3,735)
(2022,2,4,2739)
(2022,2,5,2452)
(2022,3,1,4273)
(2022,3,2,5778)
(2022,3,3,3070)
(2022,3,4,3642)
(2022,3,5,6214)
(2022,4,1,3479)
(2022,4,2,4579)
(2022,4,3,2757)
(2022,4,4,1348)
(2022,4,5,5180)
```

### run my client

this is a golden test of the results 

__do not copy my code pls__

but run this to check yours

```bash
manhours-client  -d90
```

### my client (and  your client) options

```
Test data client

Usage: manhours-client (-d|--days DAYS) [-b|--bonus]

Available options:
  -h,--help                Show this help text
  -d,--days DAYS           number of days (back from today) to go
  -b,--bonus               if to compute the bonus
```

## bonus challenge

* report also the monthly champion as the worker that tried to work on most inactive projects , together with his number of unacceptable hours
* report the champion at the end of each month manhours reports
* add year and month to this new reports
* add a string to all the tuples to distinguish the report 

### output for the client with bonus run at `lun 25 apr 2022, 16:16:48, CEST`

```haskell
("man-hours",2022,1,2,369)
("champion",2022,1,28,69)
("man-hours",2022,2,1,2774)
("man-hours",2022,2,2,1846)
("man-hours",2022,2,3,735)
("man-hours",2022,2,4,2739)
("man-hours",2022,2,5,2452)
("champion",2022,2,73,168)
("man-hours",2022,3,1,4273)
("man-hours",2022,3,2,5778)
("man-hours",2022,3,3,3070)
("man-hours",2022,3,4,3642)
("man-hours",2022,3,5,6214)
("champion",2022,3,76,64)
("man-hours",2022,4,1,3479)
("man-hours",2022,4,2,4579)
("man-hours",2022,4,3,2757)
("man-hours",2022,4,4,1348)
("man-hours",2022,4,5,5180)
("champion",2022,4,74,60)
```


